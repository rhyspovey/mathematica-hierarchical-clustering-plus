(* ::Package:: *)

(* ::Title:: *)
(*HierarchicalClusteringPlus*)


(* ::Text:: *)
(*Rhys G. Povey <rhyspovey@gmail.com>*)


(* ::Section:: *)
(*Front End*)


BeginPackage["HierarchicalClusteringPlus`",{"HierarchicalClustering`"}];


Dissimilarities::usage="Dissimilarities[cluster] returns a list of the dissimilarities in cluster data."


Deglomerate::usage="Deglomerate[cluster,threshold] separates and flattens cluster data at dissimilarities greater than threshold."


AgglomerateBy::usage="AgglomerateBy[data,function] Agglomerates data based on function.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


SowCluster3[Cluster[\[FormalX]__]]:=(Sow[{\[FormalX]}[[3]]];Cluster[\[FormalX]]);


Dissimilarities[cluster_]:=Reverse@Sort[Reap[SowCluster3@Map[SowCluster3,cluster,-2]][[2,1]]];


ClusterTest[\[FormalT]_][\[FormalX]_]:=If[Head[\[FormalX]]===Cluster,
If[\[FormalX][[3]]<\[FormalT],ClusterFlatten[\[FormalX]],MapAt[ClusterTest[\[FormalT]],\[FormalX],{{1},{2}}]],
{\[FormalX]}
];


ClusterFlatten[\[FormalX]_List]:=\[FormalX];


Deglomerate[\[FormalX]_,\[FormalT]_:\[Infinity]]:=ClusterFlatten[ClusterTest[\[FormalT]][\[FormalX]]]


AgglomerateBy[data_List,func_]:=Agglomerate[Map[func[#]->#&,data]];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
